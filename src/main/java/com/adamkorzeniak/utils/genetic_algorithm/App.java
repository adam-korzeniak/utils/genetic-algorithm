package com.adamkorzeniak.utils.genetic_algorithm;

import com.adamkorzeniak.utils.genetic_algorithm.domain.Environment;
import com.adamkorzeniak.utils.genetic_algorithm.domain.SimulationBuilder;

public class App {

    public static void main(String[] args) {
        Environment environment = null;
        var simulation = SimulationBuilder.builder(environment).build();
        simulation.simulate();
        simulation.getResults();
    }
}
