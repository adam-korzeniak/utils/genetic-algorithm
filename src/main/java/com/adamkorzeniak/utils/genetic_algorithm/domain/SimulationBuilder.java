package com.adamkorzeniak.utils.genetic_algorithm.domain;

import com.adamkorzeniak.utils.genetic_algorithm.domain.strategies.GenerationLimit;
import com.adamkorzeniak.utils.genetic_algorithm.domain.strategies.ShouldContinueStrategy;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SimulationBuilder {

    private final Environment environment;
    private ShouldContinueStrategy strategy = new GenerationLimit(100);

    public static SimulationBuilder builder(Environment environment) {
        return new SimulationBuilder(environment);
    }

    public SimulationBuilder strategy(ShouldContinueStrategy strategy) {
        this.strategy = strategy;
        return this;
    }

    public Simulation build() {
        return new Simulation(environment, strategy);
    }

}
