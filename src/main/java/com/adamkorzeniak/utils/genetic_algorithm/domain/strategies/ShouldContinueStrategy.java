package com.adamkorzeniak.utils.genetic_algorithm.domain.strategies;

import com.adamkorzeniak.utils.genetic_algorithm.domain.Simulation;

@FunctionalInterface
public interface ShouldContinueStrategy {
    boolean test(Simulation simulation);
}
