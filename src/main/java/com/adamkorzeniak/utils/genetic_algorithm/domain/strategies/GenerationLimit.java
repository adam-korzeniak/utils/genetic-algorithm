package com.adamkorzeniak.utils.genetic_algorithm.domain.strategies;

import com.adamkorzeniak.utils.genetic_algorithm.domain.Simulation;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GenerationLimit implements ShouldContinueStrategy {

    private final int generations;

    @Override
    public boolean test(Simulation simulation) {
        return simulation.getCurrentGeneration() < generations;
    }
}
