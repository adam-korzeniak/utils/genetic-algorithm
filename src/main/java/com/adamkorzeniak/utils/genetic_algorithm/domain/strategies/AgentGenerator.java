package com.adamkorzeniak.utils.genetic_algorithm.domain.strategies;

import com.adamkorzeniak.utils.genetic_algorithm.domain.Agent;
import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class AgentGenerator<T extends Agent> {

    private final int amount;

    public Set<T> generateAgents() {
        return Set.of();
    }
}
