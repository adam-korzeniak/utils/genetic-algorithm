package com.adamkorzeniak.utils.genetic_algorithm.domain;

import com.adamkorzeniak.utils.genetic_algorithm.domain.strategies.ShouldContinueStrategy;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Simulation {

    private final Environment environment;
    private final ShouldContinueStrategy shouldContinueStrategy;
    private final List<Double> averageFitnessHistory = new ArrayList<>();
    private int currentGeneration = 0;

    Simulation(Environment environment, ShouldContinueStrategy shouldContinueStrategy) {
        this.environment = environment;
        this.shouldContinueStrategy = shouldContinueStrategy;
    }

    public boolean shouldContinue() {
        return shouldContinueStrategy.test(this);
    }

    public void simulate() {
        environment.generateInitialPopulation();
        environment.calculateFitness();
        currentGeneration++;

        while (shouldContinue()) {
            environment.chooseParents();
            environment.crossover();
            environment.mutate();
            environment.calculateFitness();
            currentGeneration++;
        }
    }

    public void getResults() {
        //TODO: Implement and return results
    }

    public void addGenerationResults() {
        //TODO: implement
    }
}
