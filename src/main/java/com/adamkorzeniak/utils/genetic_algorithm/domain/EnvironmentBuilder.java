package com.adamkorzeniak.utils.genetic_algorithm.domain;

import com.adamkorzeniak.utils.genetic_algorithm.domain.strategies.*;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EnvironmentBuilder<T extends Agent> {

    private final Simulation simulation;
    private AgentGenerator<T> agentGenerator;
    private FitnessCalculator<T> fitnessCalculator;
    private AgentSelector<T> agentSelector;
    private AgentCrossover<T> agentCrossover;
    private AgentMutator<T> agentMutator;

    public static EnvironmentBuilder builder(Simulation simulation) {
        return new EnvironmentBuilder<>(simulation);
    }

    public EnvironmentBuilder<T> agentGenerator(AgentGenerator<T> agentGenerator) {
        this.agentGenerator = agentGenerator;
        return this;
    }
}
