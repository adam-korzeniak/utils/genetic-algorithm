package com.adamkorzeniak.utils.genetic_algorithm.domain.strategies;

import com.adamkorzeniak.utils.genetic_algorithm.domain.Agent;

import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

public class AgentSelector<T extends Agent> {

    public Set<T> selectParents(Set<T> agents) {
        int limit = Math.max(5, agents.size() / 100);
        return agents.stream()
                .sorted(Comparator.comparing(Agent::getFitness).reversed())
                .limit(limit)
                .collect(Collectors.toSet());
    }

}
